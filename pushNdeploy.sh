#!/bin/bash
# This script commits and pushes into the master branch, it also generates documentation and pushes them into the gh-pages branch so that they'd be displayed in the github 
# website right away.
# Upon calling this script you're expected to pass a string with a commit message like so: ./pushNdeploy.sh "I changed X and Y and Z"


git add .
git commit -m "$1"
git push

doxygen Doxyfile

cd docs
git add .
git commit -m "$1"
git push
