/*
    Copyright (C) 2015 Adham Zahran

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"

///
/// initialize a window (device), a scene manager and a GUI environment.
Main::Main()
{
	device = createDevice( video::EDT_OPENGL, dimension2d<u32>(1376, 766), 16, false, false, false, &receiver);

	if (!device)
		return;

	device->setWindowCaption(L"receiver");
	device->getCursorControl()->setVisible(false);

	driver = device->getVideoDriver();
	smgr = device->getSceneManager();
	guienv = device->getGUIEnvironment();
}

///
/// safely dispose the device
Main::~Main()
{
	device->drop();
}

//! The function where everything is initialized.
/**
here all models, scene, materials, textures, lighting and pretty much anything is initialized.
*/
void Main::init()
{	
	smgr->addCameraSceneNodeFPS(0, 100.0f, 0.01f, -1, 0, 0, false, 0.0f, false, true);
	
	device->getGUIEnvironment()->addImage(
        driver->getTexture("assets/logo.png"),
        core::position2d<s32>(10,20));

	
	sheikh = smgr->addAnimatedMeshSceneNode(smgr->getMesh("assets/shiekh.b3d"));
	if (sheikh)
	{
		sheikh->setMaterialFlag(EMF_LIGHTING, false);
		sheikh->setMaterialTexture( 0, driver->getTexture("assets/she5Texture.png") );
		sheikh->setFrameLoop(1, 1);
		//sheikh->setAnimationSpeed(24);
	}

	skybox = smgr->addAnimatedMeshSceneNode(smgr->getMesh("assets/skybox.b3d"));
	if (skybox)
	{
		skybox->setMaterialFlag(EMF_LIGHTING, false);
		skybox->setMaterialType(EMT_TRANSPARENT_ALPHA_CHANNEL_REF);
		skybox->setMaterialTexture( 0, driver->getTexture("assets/skybox1.png") );
	}

	skybox2 = smgr->addAnimatedMeshSceneNode(smgr->getMesh("assets/skybox2.b3d"));
	if (skybox2)
	{
		skybox2->setMaterialFlag(EMF_LIGHTING, false);
		skybox2->setMaterialTexture( 0, driver->getTexture("assets/skybox2.png") );
		skybox2->setFrameLoop(1, 1542);
		skybox2->setAnimationSpeed(24);
	}
}

///
/// The game main update function
void Main::update()
{
	while(device->run())
	{
		driver->beginScene(true, true, SColor(255,255,255,255));
		
		keyboard_handler();
		
		smgr->drawAll();
		guienv->drawAll();

		driver->endScene();
		display_FPS();
	}
}

//! handles keybaord input
/**
calls the EventReceiver to check keyboard input and acts accordingly.
*/
void Main::keyboard_handler()
{
	if(receiver.IsKeyDown(irr::KEY_ESCAPE))
	{
			printf("escape");
			return;
	}
	if(receiver.IsKeyDown(irr::KEY_KEY_W))
			sheikh->setFrameLoop(1, 640);
}

//! Simply displays the FPS
/**
It displays the current number of Frames per Seconds of the game right now. It is called from the update() function.
*/
void Main::display_FPS()
{
	int lastFPS = -1;
	u32 then = device->getTimer()->getTime();
	const f32 MOVEMENT_SPEED = 5.f;
	
	const u32 now = device->getTimer()->getTime();
	const f32 frameDeltaTime = (f32)(now - then) / 1000.f; // Time in seconds
	then = now;
	
	int fps = driver->getFPS();

	if (lastFPS != fps)
	{
		core::stringw tmp(L"Om 3ala2 [");
		tmp += driver->getName();
		tmp += L"] fps: ";
		tmp += fps;

		device->setWindowCaption(tmp.c_str());
		lastFPS = fps;
	}
}

//! that's the main function of the whole game
/**
This is the alpha and omega, this is the all mighty and all powerful starting point of the whole code. bow before me peasants.
*/
int main()
{
	Main game;
	game.init();
	game.update();
}
