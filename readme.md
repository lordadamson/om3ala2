\mainpage Om3ala2
Om 3ala2
========

Om 3ala2 is a game about a boy who's running away from his mother, who wants to beat him because he stepped on the rug with his dirty shoes.

Technology Used:
---------------

- Irrlicht Game Engine
- Blender

Installation
------------

just run the make file and you will find your operating system specific binary in the bin/ directory.

Contribute
----------

- Source Code: github.com/lordadamson/om3ala2


License
-------

The project is licensed under the GPL.
