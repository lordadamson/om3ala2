/*
    Copyright (C) 2015 Adham Zahran

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <irrlicht.h>

using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

#include "eventReceiver.h"

#ifdef _IRR_WINDOWS_
#pragma comment(lib, "Irrlicht.lib")
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#endif

//! This is the main class
/**
In this class you will find the main initialization function init() and the main update function update().
*/
class Main
{
private:
	IrrlichtDevice *device; //!< the main game device object.
	IVideoDriver* driver;
	ISceneManager* smgr; //!< this is the scene manager.
	IGUIEnvironment* guienv; //!< this is the GUI manager
	EventReceiver receiver; //!< Used to detect user input
			
	scene::IAnimatedMeshSceneNode* sheikh;
	scene::IAnimatedMeshSceneNode* skybox;
	scene::IAnimatedMeshSceneNode* skybox2;
	
	void display_FPS();
	void keyboard_handler();
public:
	Main();
	~Main();
	
	void init();
	void update();
};
